FROM node:10-slim

EXPOSE 80

CMD [ "node", "app/index.js" ]

WORKDIR /srv/

RUN apt-get update \
      && apt-get install -yq \
        gconf-service \
        libasound2 \
        libatk1.0-0 \
        libc6 \
        libcairo2 \
        libcups2 \
        libdbus-1-3 \
        libexpat1 \
        libfontconfig1 \
        libgcc1 \
        libgconf-2-4 \
        libgdk-pixbuf2.0-0 \
        libglib2.0-0 \
        libgtk-3-0 \
        libnspr4 \
        libpango-1.0-0 \
        libpangocairo-1.0-0 \
        libstdc++6 \
        libx11-6 \
        libx11-xcb1 \
        libxcb1 \
        libxcomposite1 \
        libxcursor1 \
        libxdamage1 \
        libxext6 \
        libxfixes3 \
        libxi6 \
        libxrandr2 \
        libxrender1 \
        libxss1 \
        libxtst6 \
        ca-certificates \
        fonts-liberation \
        libappindicator1 \
        libnss3 \
        lsb-release \
        xdg-utils \
        wget \
        fonts-thai-tlwg \
      && rm -r /var/lib/apt/lists/*

ARG node_env=production

ENV NODE_ENV=${node_env}

# cache layer
COPY package.json /tmp/package.json
COPY yarn.lock /tmp/yarn.lock
RUN cd /tmp \
  && yarn install --${node_env} \
  && mv /tmp/node_modules /srv \
  && rm package.json yarn.lock \
  && yarn cache clean

COPY . /srv/

RUN yarn install --${node_dev}
