const gulp = require('gulp');
const eslint = require('gulp-eslint');
const { spawn } = require('child_process');

const src = ['**/*.js', '!node_modules/**'];

let processInfo;

gulp.task('lint', () => gulp.src(src)
  .pipe(eslint())
  .pipe(eslint.format())
  .pipe(eslint.failAfterError()));

gulp.task('stop-dev-serve', () => {
  if (processInfo) processInfo.kill();
});

gulp.task('start-dev-serve', ['stop-dev-serve', 'lint'], () => {
  processInfo = spawn('node', ['app/index.js'], { stdio: 'inherit' });
});

gulp.task('develop', ['start-dev-serve'], () => {
  gulp.watch(src, ['start-dev-serve']);
});
