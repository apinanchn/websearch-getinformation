module.exports = {
  plugin: 'hapi-rwroute',
  options: {
    routesDir: 'app/routes',
    pathPrefix: '/api/v1',
  },
};
