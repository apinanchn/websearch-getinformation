const Boom = require('boom');
const { env } = require('process');
const good = require('./hapi-good');
const rwroute = require('./hapi-rwroute');

const plugins = [
  rwroute,
  good,
];

if (env.NODE_ENV !== 'production') {
  plugins.push({
    plugin: 'blipp',
  });
}

const server = {
  port: 80,
  host: '0.0.0.0',
  routes: {
    validate: {
      failAction: async (request, h, err) => {
        if (err.isBoom && err.isJoi && !err.isServer) {
          throw new Boom(err.details[0].message, {
            statusCode: err.output.statusCode,
          });
        } else {
          throw err;
        }
      },
    },
  },
};

module.exports = {
  server,
  register: {
    plugins,
  },
};
