const { env } = require('process');

const squeeze = {
  module: 'good-squeeze',
  name: 'Squeeze',
  args: [{
    error: '*', log: '*', request: '*', response: '*',
  }],
};

const safeJson = {
  module: 'good-squeeze',
  name: 'SafeJson',
};

const consoleOut = {
  module: 'good-console',
};

const consoleReporter = [
  squeeze,
];

if (env.NODE_ENV === 'production') {
  consoleReporter.push(safeJson);
} else {
  consoleReporter.push(consoleOut);
}
consoleReporter.push('stdout');

module.exports = {
  plugin: 'good',
  options: {
    reporters: {
      consoleReporter,
    },
  },
};
