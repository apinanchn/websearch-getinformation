const Glue = require('glue');
const authBearer = require('hapi-auth-bearer-token');
const { env } = require('process');
const manifest = require('./configs/hapi-manifest');

let server;

async function main() {
  server = await Glue.compose(manifest);

  await server.register(authBearer);
  server.auth.strategy('simple', 'bearer-access-token', {
    validate: async (request, token) => {
      const isValid = token === env.AUTH_KEY;
      const credentials = {};
      return { isValid, credentials };
    },
  });
  server.auth.default('simple');

  await server.start();
  server.log('server', 'server is running up');
}

main().catch((err) => {
  console.error(err); // eslint-disable-line no-console
  process.exit(1);
});
