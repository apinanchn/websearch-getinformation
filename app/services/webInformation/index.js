const Boom = require('boom');
const getIp = require('./ip');
const getDomainName = require('./domainName');
const getScreenshot = require('./screenshot');

async function getWebInformation(url) {
  try {
    const ips = await getIp(url);
    const domain = await getDomainName(url);
    const { domainName } = domain;
    const screenshot = await getScreenshot(url);
    return {
      id: url,
      domainName,
      ips,
      screenshot,
    };
  } catch (err) {
    return Boom.badRequest('invalid query');
  }
}

module.exports = getWebInformation;
