const dns = require('dns');
const { promisify } = require('util');
const parseDomain = require('parse-domain');
const Boom = require('boom');

const dnsAsync = promisify(dns.lookup);

const options = {
  all: true,
};

async function getIp(url) {
  try {
    const parseUrl = parseDomain(url);
    const domain = `${parseUrl.subdomain}.${parseUrl.domain}.${parseUrl.tld}`;
    return dnsAsync(domain, options);
  } catch (err) {
    throw Boom.badRequest('invalid query');
  }
}

module.exports = getIp;
