const fs = require('fs');
const puppeteer = require('puppeteer');
const { URL } = require('url');
const uuidv4 = require('uuid/v4');
const Boom = require('boom');
const config = require('./config');

async function getScreenshot(url) {
  const parsedUrl = url[0] !== 'h' ? `http://${url}` : url;
  const { hostname } = new URL(parsedUrl);
  try {
    const browser = await puppeteer.launch(config.launchOptions);
    const page = await browser.newPage();
    await page.setViewport({ width: 1080, height: 1620 });
    await page.goto(parsedUrl, { waitUntil: 'networkidle2' });
    const filename = `${hostname}-${uuidv4()}.jpeg`;
    const path = `/tmp/${filename}`;
    await page.screenshot({ path, type: 'jpeg', quality: 30 });
    await browser.close();
    return {
      base64Image: Buffer.from(fs.readFileSync(path)).toString('base64'),
      filename,
    };
  } catch (err) {
    throw Boom.badRequest('invalid query');
  }
}

module.exports = getScreenshot;
