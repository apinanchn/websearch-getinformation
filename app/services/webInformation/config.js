module.exports = {
  launchOptions: (() => {
    const options = {};
    options.args = ['--no-sandbox', '--disable-setuid-sandbox'];
    return options;
  })(),
};
