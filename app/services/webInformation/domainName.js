const whois = require('whois-json');
const parseDomain = require('parse-domain');
const Boom = require('boom');

async function getDomainName(url) {
  try {
    const parseUrl = parseDomain(url);
    const domain = `${parseUrl.domain}.${parseUrl.tld}`;
    return whois(domain);
  } catch (err) {
    throw Boom.badRequest('invalid query');
  }
}

module.exports = getDomainName;
