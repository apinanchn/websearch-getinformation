const axios = require('axios');
const { env } = require('process');
const Boom = require('boom');

async function googleCustomSearch(keyword, indexOfFirstResult) {
  try {
    const results = await axios.get('https://www.googleapis.com/customsearch/v1', {
      params: {
        key: env.GOOGLE_KEY,
        cx: env.GOOGLE_CX,
        q: keyword,
        start: indexOfFirstResult,
      },
    });
    return results.data.items.map(result => ({
      link: result.link,
    }));
  } catch (err) {
    return Boom.badImplementation(err);
  }
}

module.exports = googleCustomSearch;
