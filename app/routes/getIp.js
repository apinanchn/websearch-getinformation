const Joi = require('joi');
const getIp = require('../services/webInformation/ip');

async function getIpHandler(request) {
  return getIp(request.query.url);
}

function getRouteOptions() {
  return {
    validate: {
      query: {
        url: Joi.string().uri({
          scheme: [
            /^http/,
            /^https/,
          ],
        }).required(),
      },
    },
  };
}

module.exports = [
  {
    options: getRouteOptions(),
    handler: getIpHandler,
    method: 'GET',
  },
];
