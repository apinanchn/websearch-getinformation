const Joi = require('joi');
const getWebInformation = require('../services/webInformation');

async function getWebInformationHandler(request) {
  return getWebInformation(request.query.url);
}

function getRouteOptions() {
  return {
    validate: {
      query: {
        url: Joi.string().uri({
          scheme: [
            /^http/,
            /^https/,
          ],
        }).required(),
      },
    },
  };
}

module.exports = [
  {
    options: getRouteOptions(),
    handler: getWebInformationHandler,
    method: 'GET',
  },
];
