const Joi = require('joi');
const getDomainName = require('../services/webInformation/domainName');

async function getDomainNameHandler(request) {
  return getDomainName(request.query.url);
}

function getRouteOptions() {
  return {
    validate: {
      query: {
        url: Joi.string().uri({
          scheme: [
            /^http/,
            /^https/,
          ],
        }).required(),
      },
    },
  };
}

module.exports = [
  {
    options: getRouteOptions(),
    handler: getDomainNameHandler,
    method: 'GET',
  },
];
