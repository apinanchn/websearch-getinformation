const Joi = require('joi');
const getScreenshot = require('../services/webInformation/screenshot');

async function getScreenshotHandler(request) {
  return getScreenshot(request.query.url);
}

function getRouteOptions() {
  return {
    validate: {
      query: {
        url: Joi.string().uri({
          scheme: [
            /^http/,
            /^https/,
          ],
        }).required(),
      },
    },
  };
}

module.exports = [
  {
    options: getRouteOptions(),
    handler: getScreenshotHandler,
    method: 'GET',
  },
];
