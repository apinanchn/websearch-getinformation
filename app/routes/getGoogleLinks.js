const Joi = require('joi');
const googleCustomSearch = require('../services/googleCustomSearch');

async function getGoogleLinksHandler(request) {
  return googleCustomSearch(request.query.keyword, request.query.indexOfResult);
}

function getRouteOptions() {
  return {
    validate: {
      query: {
        keyword: Joi.string().required(),
        indexOfResult: Joi.number().integer().required(),
      },
    },
  };
}

module.exports = [
  {
    options: getRouteOptions(),
    handler: getGoogleLinksHandler,
    method: 'GET',
  },
];
